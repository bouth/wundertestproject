package com.bouthaina.wundertestproject.di.module;


import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.bouthaina.wundertestproject.di.util.ViewModelKey;
import com.bouthaina.wundertestproject.ui.carDetails.CarDetailsFragmentViewModel;
import com.bouthaina.wundertestproject.util.ViewModelFactory;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Singleton
@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(CarDetailsFragmentViewModel.class)
    abstract ViewModel bindListViewModel(CarDetailsFragmentViewModel carDetailsFragmentViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);
}
