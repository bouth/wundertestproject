package com.bouthaina.wundertestproject.di.module;


import com.bouthaina.wundertestproject.ui.main.MainActivity;
import com.bouthaina.wundertestproject.ui.main.MainFragmentBindingModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {
    @ContributesAndroidInjector(modules = {MainFragmentBindingModule.class})
    abstract MainActivity bindMainActivity();
}
