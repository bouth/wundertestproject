package com.bouthaina.wundertestproject.di.util;


import android.databinding.BindingAdapter;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bouthaina.wundertestproject.R;
import com.bouthaina.wundertestproject.base.BaseApplication;
import com.bumptech.glide.Glide;

public class AndroidDataBindingAdapters {

    @BindingAdapter({"srcUrl"})
    public static void setImageResource(final ImageView imageView, String srcUrl) {
        imageView.setTag(imageView.getId(), srcUrl);
            Glide.with(BaseApplication.getInstance()).load(srcUrl).into(imageView);
        Animation aniFade = AnimationUtils.loadAnimation(BaseApplication.getContext().getApplicationContext(), R.anim.fade_in);
        imageView.startAnimation(aniFade);
    }


    @BindingAdapter("visibleElseGone")
    public static void visible(View view, boolean visible) {
        view.setVisibility(visible ? View.VISIBLE : View.GONE);
    }
}
