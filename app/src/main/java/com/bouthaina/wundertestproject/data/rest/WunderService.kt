package com.bouthaina.wundertestproject.data.rest

import com.bouthaina.wundertestproject.data.model.Car
import retrofit2.Call
import retrofit2.http.*

/**
 * API retrrofits 2
 *
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */
interface WunderService {

    @get:GET("wunderfleet-recruiting-dev/cars.json")
    val getJSON: Call<List<Car>>


    @GET("wunderfleet-recruiting-dev/cars/{carId}")
    fun getCarDetails(@Path("carId") carId : Int): Call<Car>


    @Headers("Authorization: Bearer df7c313b47b7ef87c64c0f5f5cebd6086bbb0fa")
    @POST("/default/wunderfleet-recruiting-mobile-dev-quick-rental")
    fun startRunt(@Body carId: Int) : Call<Car>
}
