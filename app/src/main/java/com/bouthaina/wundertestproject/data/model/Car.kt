package com.bouthaina.wundertestproject.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Wunder model to represent a car
 *
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */
class Car(
        @SerializedName("carId")
        val carId: Int,
        @SerializedName("title")
        val title: String ="",
        @SerializedName("lat")
        val lat: Double,
        @SerializedName("lon")
        val lon: Double,
        @SerializedName("licencePlate")
        val licencePlate: String="",
        @SerializedName("fuelLevel")
        val fuelLevel: Int,
        @SerializedName("vehicleStateId")
        val vehicleStateId: Int,
        @SerializedName("vehicleTypeId")
        val vehicleTypeId: Int,
        @SerializedName("pricingTime")
        val pricingTime: String="",
        @SerializedName("pricingParking")
        val pricingParking: String="",
        @SerializedName("reservationState")
        val reservationState: Int,
        @SerializedName("isClean")
        val isClean: Boolean,
        @SerializedName("isDamaged")
        val isDamaged: Boolean,
        @SerializedName("distance")
        val distance: String="",
        @SerializedName("address")
        val address: String="",
        @SerializedName("zipCode")
        val zipCode: String="",
        @SerializedName("city")
        val city: String,
        @SerializedName("locationId")
        val locationId: Int,
        @SerializedName("damageDescription")
        val damageDescription :String="",
        @SerializedName("vehicleTypeImageUrl")
        val vehicleTypeImageUrl :String="",
        @SerializedName("isActivatedByHardware")
        val isActivatedByHardware: Boolean,
        @SerializedName("hardwareId")
        val hardwareId :String=""
) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readDouble(),
            parcel.readDouble(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            parcel.readString()
            )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(carId)
        parcel.writeString(title)
        parcel.writeDouble(lat)
        parcel.writeDouble(lon)
        parcel.writeString(licencePlate)
        parcel.writeInt(fuelLevel)
        parcel.writeInt(vehicleStateId)
        parcel.writeInt(vehicleTypeId)
        parcel.writeString(pricingTime)
        parcel.writeString(pricingParking)
        parcel.writeInt(reservationState)
        parcel.writeByte(if (isClean) 1 else 0)
        parcel.writeByte(if (isDamaged) 1 else 0)
        parcel.writeString(distance)
        parcel.writeString(address)
        parcel.writeString(zipCode)
        parcel.writeString(city)
        parcel.writeInt(locationId)
        parcel.writeString(damageDescription)
        parcel.writeString(vehicleTypeImageUrl)
        parcel.writeByte(if (isActivatedByHardware) 1 else 0)
        parcel.writeString(hardwareId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Car> {
        override fun createFromParcel(parcel: Parcel): Car {
            return Car(parcel)
        }

        override fun newArray(size: Int): Array<Car?> {
            return arrayOfNulls(size)
        }
    }
}