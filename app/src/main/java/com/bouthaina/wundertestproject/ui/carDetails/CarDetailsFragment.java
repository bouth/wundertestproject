package com.bouthaina.wundertestproject.ui.carDetails;


import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bouthaina.wundertestproject.R;
import com.bouthaina.wundertestproject.base.BaseApplication;
import com.bouthaina.wundertestproject.base.BaseFragment;
import com.bouthaina.wundertestproject.databinding.CarDetailsBinding;
import com.bouthaina.wundertestproject.util.ViewModelFactory;

import javax.inject.Inject;

/**
 * Represents the details of selected car
 *
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */
public class CarDetailsFragment extends BaseFragment {

    @Inject
    ViewModelFactory viewModelFactory;
    private CarDetailsFragmentViewModel viewModel;
    private CarDetailsBinding binding ;

    @Override
    protected int layoutRes() {
        return R.layout.car_details;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.setContentView(BaseApplication.getInstance().getCurrentActivity(), R.layout.car_details);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(CarDetailsFragmentViewModel.class);
        binding.setViewModel(viewModel);
        viewModel.loadCarDetails((int) this.getArguments().get("carId"));
    }
}
