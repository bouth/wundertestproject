package com.bouthaina.wundertestproject.ui.carDetails

import android.app.AlertDialog
import android.arch.lifecycle.ViewModel
import android.content.DialogInterface
import android.util.Log
import com.bouthaina.wundertestproject.base.BaseApplication
import com.bouthaina.wundertestproject.data.model.Car
import com.bouthaina.wundertestproject.data.rest.Repository
import com.bouthaina.wundertestproject.data.rest.WunderService
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject
import android.view.LayoutInflater
import com.bouthaina.wundertestproject.R
import kotlinx.android.synthetic.main.custom_alert.view.*

/**
 * ViewModel for {@link CarDetailsFragment}.
 *
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */
@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class CarDetailsFragmentViewModel @Inject constructor(private val repository: Repository) : ViewModel() {

    var bindingModel: CarDetailsFragmentBindingModel = CarDetailsFragmentBindingModel()

    fun loadCarDetails(carId :Int) {
        run {
            val retrofit = Retrofit.Builder().baseUrl("https://s3.eu-central-1.amazonaws.com/").addConverterFactory(GsonConverterFactory.create()).build()
            val requestInterface = retrofit.create(WunderService::class.java)

            val call = requestInterface.getCarDetails(carId = carId ).also {
                it.run {
                    enqueue(object : retrofit2.Callback<Car> {
                        override fun onFailure(call: retrofit2.Call<Car>, t: Throwable) {
                            Log.e("Error", t.message)
                            showErrorDialog(" Sorry ! there is an error in loading Data for this car please try later!")
                        }

                        override fun onResponse(call: retrofit2.Call<Car>, response: Response<Car>) {
                            if(response.body() != null)  bindingModel.car  = response.body()
                            if (bindingModel.car == null ) showErrorDialog(" Sorry ! there is an error in loading Data for this car please try later!")
                        }
                    })
                }
            }
        }
    }

    private fun  showErrorDialog(textMesage :String){
        val alertDialog = AlertDialog.Builder(BaseApplication.getInstance().currentActivity)
                .setTitle("ERROR")
                .setMessage(textMesage)
                    .setNegativeButton("OK", DialogInterface.OnClickListener { dialog, id -> dialog.cancel()
                        BaseApplication.getInstance().currentActivity.onBackPressed()
                    })
                    alertDialog.show()
    }

    fun startRintCar(){
        run {
            val retrofit = Retrofit.Builder().baseUrl("https://s3.eu-central-1.amazonaws.com/").addConverterFactory(GsonConverterFactory.create()).build()
            val requestInterface = retrofit.create(WunderService::class.java)

            val call = requestInterface.startRunt(carId = bindingModel.car.carId ).also {
                it.run {
                    enqueue(object : retrofit2.Callback<Car> {
                        override fun onFailure(call: retrofit2.Call<Car>, t: Throwable) {
                            showErrorDialog("runt car failed")
                        }

                        override fun onResponse(call: retrofit2.Call<Car>, response: Response<Car>) {
                            val car = response.body()
                            showCustomDialog(car)
                        }
                    })
                }
            }
        }
    }

    private fun showCustomDialog(car:Car?) {

        val mDialogView = LayoutInflater.from(BaseApplication.getInstance().currentActivity).inflate(R.layout.custom_alert ,null)
        //AlertDialogBuilder
        val mBuilder = AlertDialog.Builder(BaseApplication.getInstance().currentActivity).setView(mDialogView)
        //show dialog
        val  mAlertDialog = mBuilder.show()
        //login button click of custom layout
        mDialogView.okBtn.setOnClickListener {
            //dismiss dialog
            mAlertDialog.dismiss()
        }
    }
}


