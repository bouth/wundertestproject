package com.bouthaina.wundertestproject.ui.carDetails;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.bouthaina.wundertestproject.data.model.Car;

/**
 * Two-Way binding model for {@link CarDetailsFragment}
 *
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */
public class CarDetailsFragmentBindingModel extends BaseObservable {
    private Car car ;
    private String notAvailable ;

    @Bindable
    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
        notifyPropertyChanged(com.bouthaina.wundertestproject.BR.car);
    }

    public String getNotAvailable() {
        return "NA";
    }
}
