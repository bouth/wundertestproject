package com.bouthaina.wundertestproject.ui.main;

import com.bouthaina.wundertestproject.ui.carDetails.CarDetailsFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MainFragmentBindingModule {

    @ContributesAndroidInjector
    abstract CarDetailsFragment provideListFragment();
}
