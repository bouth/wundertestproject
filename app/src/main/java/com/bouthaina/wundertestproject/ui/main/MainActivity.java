package com.bouthaina.wundertestproject.ui.main;


import android.os.Bundle;

import com.bouthaina.wundertestproject.R;
import com.bouthaina.wundertestproject.base.BaseActivity;
import com.bouthaina.wundertestproject.base.BaseFragment;
import com.bouthaina.wundertestproject.ui.carDetails.CarDetailsFragment;

import java.util.Observable;

/**
 * main activity
 *
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */
public class MainActivity extends BaseActivity {
    public int carId = 0;

    @Override
    protected int layoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            carId = bundle.getInt("carId");
        }

        bundle.putInt("carId", carId);
        BaseFragment fragment = new CarDetailsFragment();
        fragment.setArguments(bundle);
        if(savedInstanceState == null)
            getSupportFragmentManager().beginTransaction().add(R.id.screenContainer, fragment).commit();
    }

    @Override
    public void update(Observable observable, Object o) {
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
    }
}