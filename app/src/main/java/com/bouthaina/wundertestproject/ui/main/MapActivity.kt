package com.bouthaina.wundertestproject.ui.main

import android.content.Intent
import android.os.Bundle

import com.bouthaina.wundertestproject.R

import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment

import android.support.v4.app.FragmentActivity

import android.util.Log
import com.bouthaina.wundertestproject.data.model.Car
import com.bouthaina.wundertestproject.data.rest.WunderService
import com.google.android.gms.maps.OnMapReadyCallback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.*


/**
 * this activity for showing different car location in the map
 *
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */
class MapActivity : FragmentActivity() {

    lateinit var mMap: GoogleMap
    lateinit var mapFragment: SupportMapFragment
    private var carLists = ArrayList<Car>()
    private var markers = ArrayList<Marker>()

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.map_screen)
        mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment

        loadData()
        mapFragment.getMapAsync { googleMap ->
            mMap = googleMap
            mMap.setOnMapLoadedCallback { mMap = googleMap }
        }
    }

    fun loadData() {
        run {
            val retrofit = Retrofit.Builder().baseUrl("https://s3.eu-central-1.amazonaws.com/").addConverterFactory(GsonConverterFactory.create()).build()
            val requestInterface = retrofit.create(WunderService::class.java)

            val call = requestInterface.getJSON.also {
                it.run {
                    enqueue(object : retrofit2.Callback<List<Car>>, OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
                        override fun onMarkerClick(p0: Marker): Boolean {
                            var clickCount = p0.tag as Int
                             clickCount  += 1
                            p0.tag = clickCount
                            if(clickCount == 1){
                                for ( i in markers) {
                                  if (p0 != i)  i.isVisible = false
                                }
                            }
                            if (clickCount >1) {
                                val myIntent = Intent(this@MapActivity, MainActivity::class.java)
                                myIntent.putExtra("carId",carLists[(markers.indexOf(p0))].carId) //Optional parameters
                                this@MapActivity.startActivity(myIntent)
                            }

                            return false
                        }

                        override fun onMapReady(p0: GoogleMap?) {
                            mMap = p0!!
                            for (i in 0 until carLists.size) {
                                markers.add(mMap.addMarker(MarkerOptions().position(LatLng(carLists[i].lat , carLists[i].lon))
                                .title(carLists[i].title)))
                                markers[i].tag = 0
                                markers[i].showInfoWindow()
                            }

                            val googlePlex = CameraPosition.builder().target(LatLng(carLists[7].lat, carLists[7].lon)).zoom(15f).bearing(0f).tilt(45f).build()
                            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(googlePlex), 1000, null)

                            mMap.setOnMarkerClickListener(this)

                        }

                        override fun onFailure(call: retrofit2.Call<List<Car>>, t: Throwable) {
                            Log.e("Error", t.message)
                        }

                        override fun onResponse(call: retrofit2.Call<List<Car>>, response: Response<List<Car>>) {
                            val list = response.body()
                            if (response.body() != null) carLists.addAll(list!!)
                            mapFragment.getMapAsync(this)
                        }
                    })
                }
            }
        }
    }

}
