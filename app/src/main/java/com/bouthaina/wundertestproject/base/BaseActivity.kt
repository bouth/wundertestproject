package com.bouthaina.wundertestproject.base

import android.os.Bundle
import android.support.annotation.LayoutRes
import butterknife.ButterKnife
import dagger.android.support.DaggerAppCompatActivity
import java.util.*

/**
 * BaseActivity for all Activities.
 *
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */
abstract class BaseActivity : DaggerAppCompatActivity() , Observer {

    @LayoutRes
    protected abstract fun layoutRes(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        BaseApplication.getInstance().currentActivity = this
        setContentView(layoutRes())
        ButterKnife.bind(this)
    }
}