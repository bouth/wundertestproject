package com.bouthaina.wundertestproject.base;


import android.app.Activity;
import android.content.Context;

import com.bouthaina.wundertestproject.di.compoent.ApplicationComponent;
import com.bouthaina.wundertestproject.di.compoent.DaggerApplicationComponent;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;

/**
 * Application which handles Services and other Setups before starting the first Activity
 *
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */
public class BaseApplication  extends DaggerApplication {

        private static Context context;
        private Activity mCurrentActivity = null;
        private static BaseApplication mInstance;


        @Override
        public void onCreate() {
            super.onCreate();
            mInstance = this;
            context = getApplicationContext();
        }

        @Override
        protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
            ApplicationComponent component = DaggerApplicationComponent.builder().application(this).build();
            component.inject(this);

            return component;
        }

        public static Context getContext() {
            return context;
        }


        public Activity getCurrentActivity() {
            return mCurrentActivity;
        }

        public void setCurrentActivity(Activity mCurrentActivity) {
            this.mCurrentActivity = mCurrentActivity;
        }

        /**
         * Return the application instance.
         * No need to instantiate explicitly!
         *
         * @return
         */
        public static BaseApplication getInstance() {
            return mInstance;
        }
    }
